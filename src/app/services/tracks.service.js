class TracksService {

  constructor (Restangular, $q) {
    'ngInject';

    this.Restangular = Restangular;
    this.$q = $q;
  }

  fetch(tracks){
    const RestangularSpotify = this.Restangular.withConfig(RestangularConfigurer => RestangularConfigurer.setBaseUrl('https://api.spotify.com/'));
    const api = RestangularSpotify.all('v1');

    return this.$q.all(tracks.map(track => {
      return api.get('search', { type: 'track', q: track});
    }))
    .then(responses => {
      return responses.map(response => {
        return response.tracks.items
        .filter(item => item.available_markets.indexOf('NL') !== -1)
        .map(item => item.uri)
        [0];
      })
      .filter(track => typeof(track) !== 'undefined');
    });
  }
}

export default TracksService;
