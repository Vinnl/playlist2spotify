class MainController {
  constructor ($scope, tracksService) {
    'ngInject';

    this.tracksService = tracksService;

    this.tracks = [];

    $scope.$watch(
      () => this.tracks,
      newTracks => {
        tracksService.fetch(newTracks)
        .then(urls => {
          this.urls = urls.reduce((soFar, url) => {
            return `${soFar} ${url}`;
          }, '').substr(1);
        });
      }
    );
    
    this.urls = '';
  }

  trackList(newTracklist){
    if(undefined === newTracklist){
      return this.tracks.reduce((soFar, track) => `${soFar}\n${track}`, '').substr(1);
    }
    
    this.tracks = newTracklist.split(/\r\n|\r|\n/g);
  }
}

export default MainController;
