import config from './index.config';

import runBlock from './index.run';
import MainController from './main/main.controller';
import TracksService from '../app/services/tracks.service';

angular.module('playlist2spotify', ['ngAnimate', 'ngTouch', 'restangular', 'mm.foundation'])
  .config(config)

  .run(runBlock)
  .service('tracksService', TracksService)
  .controller('MainController', MainController);
